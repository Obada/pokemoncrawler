from django.conf.urls import url
from . import views

urlpatterns = [
    url('^list$', views.PokemonListViewSet.as_view({'get': 'list'})),
]
