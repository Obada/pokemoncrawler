from django.test import TestCase
from unittest.mock import patch
from .models import Pokemon
from pokemon.management.commands.runcrawler import PokeApiCrawler, BASE_URL


class PokemonModelTest(TestCase):

    def test_ingest_create(self):
        sample_data = {
            'name': 'Obada',
            'height': 185,
            'weight': 90,
            'extra': {}
        }
        Pokemon.ingest(sample_data)
        # test create
        self.assertTrue(Pokemon.objects.count() == 1)

    def test_ingest_update(self):
        sample_data = {
            'name': 'Obada',
            'height': 185,
            # got fatter :(
            'weight': 100,
            'extra': {}
        }
        Pokemon.ingest(sample_data)
        # test create
        self.assertTrue(Pokemon.objects.count() == 1)
        self.assertTrue(Pokemon.objects.first().weight == 100)


class RunCrawlerTests(TestCase):
    @patch('requests.get')
    def test_next(self, get):
        # ApiCrawler will re-set `next_url` to BASE_URL to maintain an infinite loop;
        crawler = PokeApiCrawler()
        self.assertTrue(crawler.next_url is None)
        crawler.next()
        self.assertTrue(get.called)
        # Should be set with base-url
        self.assertFalse(crawler.next_url is None)

    @patch('requests.get')
    @patch('pokemon.models.Pokemon.ingest')
    def test_update_or_create(self, ingest, get):
        PokeApiCrawler().update_or_create('')
        self.assertTrue(ingest.called and get.called)