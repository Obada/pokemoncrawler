from django.db import models
from django.db.models import JSONField


class Pokemon(models.Model):
    # the longest pokemon name cannot exceed 11 chars
    # db_index & unique for faster lookups.
    name = models.CharField(max_length=11, unique=True, db_index=True)

    last_updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    weight = models.IntegerField()
    height = models.IntegerField()

    extra = JSONField('extra', help_text='API raw response')

    @classmethod
    def ingest(cls, data):
        """
        Ingest data coming from the web crawler
        """

        pokemon_qs = cls.objects.filter(name=data['name'])
        if pokemon_qs.exists():
            pokemon = pokemon_qs.first()
        else:
            Pokemon.objects.create(
                name=data['name'], weight=data['weight'],
                height=data['height'], extra=data)
            return

        to_update = ['extra']
        pokemon.extra = data

        if pokemon.height != data['height']:
            to_update.append('height')
            pokemon.height = data['height']

        if pokemon.weight != data['weight']:
            to_update.append('weight')
            pokemon.weight = data['weight']

        pokemon.save(update_fields=to_update)

    def __str__(self):
        return self.name