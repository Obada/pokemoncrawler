from django.shortcuts import render
from rest_framework import viewsets
from .models import Pokemon
from .serializers import PokemonListSerializer
from rest_framework.pagination import CursorPagination


class PokemonListViewSet(viewsets.ModelViewSet):
    serializer_class = PokemonListSerializer
    queryset = Pokemon.objects.all()
    page_size = 5