import logging
import requests
from django.db import connection

from threading import Thread
from django.conf import settings
from django.core.management.base import BaseCommand
from pokemon.models import Pokemon


logger = logging.getLogger('crawler')

BASE_URL = 'https://pokeapi.co/api/v2/pokemon/?limit={thread_count}'


class PokeApiCrawler(object):

    thread_count = 5
    next_url = None

    def __init__(self, thread_count=None):
        if thread_count:
            self.thread_count = thread_count

    def next(self):
        # next set of URLs to crawl (infinite: will go back to base)
        if self.next_url is None:
            self.next_url = BASE_URL.format(thread_count=self.thread_count)

        response = requests.get(self.next_url)
        try:
            response.raise_for_status()
        except requests.HTTPError:
            logger.exception('CrawlerMainThread: terminating')
            return []

        data = response.json()
        self.next_url = data['next']
        # didn't avoid KeyError here (any failure will be logged from the Thread)
        return [x['url'] for x in data['results']]

    @staticmethod
    def thread_run(url):
        try:
            PokeApiCrawler.update_or_create(url)
        except:
            logger.exception('ThreadFailure: terminated')
        finally:
            # make sure to close the connection on thread termination
            connection.close()

    @staticmethod
    def update_or_create(url):
        response = requests.get(url)
        try:
            response.raise_for_status()
        except requests.HTTPError:
            logger.exception('HTTPError:thread-terminated')
            return

        data = response.json()
        # transaction.atomic if we turn this into multi-process design?
        Pokemon.ingest(data)

    def run(self):
        """
        With Django & thread concurrency in Python, it should be save to use threads here;
        since each thread will have its own connection to the DB and since we have one process running
        of `runcrawler` each thread should update only one Pokemon object.
        """
        threads = []
        # Looks scary, I know.
        while True:
            for url in self.next():
                thread = Thread(target=self.thread_run, args=(url,))
                thread.start()
                threads.append(thread)
                # wait for running threads to finish to start with a new iteration.
                [t.join() for t in threads]


class Command(BaseCommand):
    """
    Administrative command to actively check for Pokemons using `https://pokeapi.co`

    - On local env: This will run on a docker container.
    - On production: This can run on:
                    A - supervisorctl process on its own EC2
                    B - Wrapped in a Celery task as a crontab job (this requires a different design/implementation)
                    Personally going with option (A) since the entire "project" is designed to simply
                    crawl `https://pokeapi.co`;
    """

    def add_arguments(self, parser):
        # increase or decrease thread count
        parser.add_argument(
            '--threads',
            type=int,
            help='number of threads',
            default=5
        )

    def handle(self, **kw):
        logger.info('CrawlerService: started')
        try:
            PokeApiCrawler(kw['threads']).run()
        except:
            logger.exception('CrawlerService')
