from rest_framework import serializers
from .models import Pokemon


class PokemonListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pokemon
        fields = ('id', 'name', 'weight', 'height', 'last_updated')
